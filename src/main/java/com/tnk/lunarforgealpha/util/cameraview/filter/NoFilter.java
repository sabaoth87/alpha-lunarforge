package com.tnk.lunarforgealpha.util.cameraview.filter;

import androidx.annotation.NonNull;

public class NoFilter extends BaseFilter {

    @NonNull
    @Override
    public String getFragmentShader() {
        return createDefaultFragmentShader();
    }
}
