package com.tnk.lunarforgealpha.util.cameraview.preview;

/**
 * Indicates that some action is being executed on the renderer thread.
 */
public @interface RendererThread {}
