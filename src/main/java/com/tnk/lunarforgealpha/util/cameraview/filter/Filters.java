package com.tnk.lunarforgealpha.util.cameraview.filter;

import androidx.annotation.NonNull;

import com.tnk.lunarforgealpha.util.cameraview.filters.AutoFixFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.BlackAndWhiteFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.BrightnessFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.ContrastFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.CrossProcessFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.DocumentaryFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.DuotoneFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.FillLightFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.GammaFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.GrainFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.GrayscaleFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.HueFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.InvertColorsFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.LomoishFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.PosterizeFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.SaturationFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.SepiaFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.SharpnessFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.TemperatureFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.TintFilter;
import com.tnk.lunarforgealpha.util.cameraview.filters.VignetteFilter;

/**
 * Contains commonly used {@link Filter}s.
 *
 * You can use {@link #newInstance()} to create a new instance and
 * pass it to {@link com.tnk.lunarforgealpha.util.cameraview.CameraView#setFilter(Filter)}.
 */
public enum Filters {

    /** @see NoFilter */
    NONE(NoFilter.class),

    /** @see AutoFixFilter */
    AUTO_FIX(AutoFixFilter.class),

    /** @see BlackAndWhiteFilter */
    BLACK_AND_WHITE(BlackAndWhiteFilter.class),

    /** @see BrightnessFilter */
    BRIGHTNESS(BrightnessFilter.class),

    /** @see ContrastFilter */
    CONTRAST(ContrastFilter.class),

    /** @see CrossProcessFilter */
    CROSS_PROCESS(CrossProcessFilter.class),

    /** @see DocumentaryFilter */
    DOCUMENTARY(DocumentaryFilter.class),

    /** @see DuotoneFilter */
    DUOTONE(DuotoneFilter.class),

    /** @see FillLightFilter */
    FILL_LIGHT(FillLightFilter.class),

    /** @see GammaFilter */
    GAMMA(GammaFilter.class),

    /** @see GrainFilter */
    GRAIN(GrainFilter.class),

    /** @see GrayscaleFilter */
    GRAYSCALE(GrayscaleFilter.class),

    /** @see HueFilter */
    HUE(HueFilter.class),

    /** @see InvertColorsFilter */
    INVERT_COLORS(InvertColorsFilter.class),

    /** @see LomoishFilter */
    LOMOISH(LomoishFilter.class),

    /** @see PosterizeFilter */
    POSTERIZE(PosterizeFilter.class),

    /** @see SaturationFilter */
    SATURATION(SaturationFilter.class),

    /** @see SepiaFilter */
    SEPIA(SepiaFilter.class),

    /** @see SharpnessFilter */
    SHARPNESS(SharpnessFilter.class),

    /** @see TemperatureFilter */
    TEMPERATURE(TemperatureFilter.class),

    /** @see TintFilter */
    TINT(TintFilter.class),

    /** @see VignetteFilter */
    VIGNETTE(VignetteFilter.class);

    private Class<? extends Filter> filterClass;

    Filters(@NonNull Class<? extends Filter> filterClass) {
        this.filterClass = filterClass;
    }

    /**
     * Returns a new instance of the given filter.
     * @return a new instance
     */
    @NonNull
    public Filter newInstance() {
        try {
            return filterClass.newInstance();
        } catch (IllegalAccessException e) {
            return new NoFilter();
        } catch (InstantiationException e) {
            return new NoFilter();
        }
    }
}
