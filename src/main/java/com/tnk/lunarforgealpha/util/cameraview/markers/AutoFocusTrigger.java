package com.tnk.lunarforgealpha.util.cameraview.markers;

import com.tnk.lunarforgealpha.util.cameraview.CameraView;
import com.tnk.lunarforgealpha.util.cameraview.gesture.GestureAction;

/**
 * Gives information about what triggered the autofocus operation.
 */
public enum AutoFocusTrigger {

    /**
     * Autofocus was triggered by {@link GestureAction#AUTO_FOCUS}.
     */
    GESTURE,

    /**
     * Autofocus was triggered by the {@link CameraView#startAutoFocus(float, float)} method.
     */
    METHOD
}
