package com.tnk.lunarforgealpha.util.fd

data class FaceDetectionModel(val id: Int, val text: String?)
