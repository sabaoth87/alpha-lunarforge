package com.tnk.lunarforgealpha

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.tnk.lunarforgealpha.dev.TESTActFD
import com.unity3d.player.R

import kotlinx.android.synthetic.main.activity_launcher.*
// Kotlin Tutorial

class LauncherActivity : AppCompatActivity() {
    /** 2019-08-13 - 9:22 PM
    @author Be4stly
    ::  "From the Kotlin Android Extensions Tutorial"
    {@link #onCreate(savedInstanceState: Bundle?) onCreate}
     */

    private val tag: String = "LauncherActivity"

    lateinit private var buttonUnityLauncher: ImageButton
    lateinit private var buttonFaceDetectLauncher: ImageButton
    lateinit private var buttonFaceDetectTestLauncher: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)


        buttonUnityLauncher = findViewById<ImageButton>(R.id.btn_unity)
        buttonFaceDetectLauncher = findViewById<ImageButton>(R.id.btn_faced)
        buttonFaceDetectTestLauncher = findViewById<ImageButton>(R.id.btn_facetest)


        buttonUnityLauncher.setOnClickListener(View.OnClickListener {
            val intentUnity = Intent(this, ActUnityPlayer::class.java)
            startActivity(intentUnity)
        })

        buttonFaceDetectLauncher.setOnClickListener(View.OnClickListener {
            val intentFD = Intent(this, ActFaceDetect::class.java)
            startActivity(intentFD)
        })

        buttonFaceDetectTestLauncher.setOnClickListener(View.OnClickListener {
            val intentTest = Intent(this, TESTActFD::class.java)
            startActivity(intentTest)
        })

        // TEMPLATE DEFAULT
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        // TEMPLATE END
    }

}
