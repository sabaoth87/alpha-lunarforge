package com.tnk.lunarforgealpha.dev

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Bundle
import android.text.TextPaint
import android.util.DisplayMetrics
import android.util.Log

import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions

import com.tnk.lunarforgealpha.util.cameraview.controls.Facing
import com.tnk.lunarforgealpha.util.cameraview.frame.Frame
import com.tnk.lunarforgealpha.util.cameraview.frame.FrameProcessor
import com.unity3d.player.R
import kotlinx.android.synthetic.main.con__test_face_detect.*

// CameraX Permissions
// This is an arbitrary number we are using to keep tab of the permission
// request. Where an app has multiple context for requesting permission,
// this can help differentiate the different contexts
private const val REQUEST_CODE_PERMISSIONS = 10

// This is an array of all the permission specified in the manifest
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)


class TESTActFD : AppCompatActivity(), FrameProcessor, LifecycleOwner {

    private var cameraFacing: Facing = Facing.BACK
    private var faceSize = 0.0f

    private var faceNear = false
    private var faceMid = false
    private var faceFar = false

    val faceSizeNearLim = 150000.0
    val faceSizeMidLo =  75000.0
    val faceSizeMidHi =  85000.0
    val faceSizeFarLim = 4000.0


    private val imageView by lazy { findViewById<ImageView>(R.id.fd_imgv)!! }

    init {
        Log.v("TestAnalyzer","First init!")
    }

    // High-accuracy landmark detection and face classification

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act__test_face_detect)


        //
        fd_camv.facing = cameraFacing
        fd_camv.setLifecycleOwner(this)
        fd_camv.addFrameProcessor(this)

        // CameraX Permissions
        // Add this at the end of onCreate function

        // Request camera permissions
        if (allPermissionsGranted()) {
            imageView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        // Every time the provided texture view changes, recompute layout
        imageView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }

    override fun process(frame: Frame) {

        val display = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(display)

        val displayWidth = display.widthPixels
        val displayHeight = display.heightPixels

        val width = frame.size.width
        val height = frame.size.height

        val metadata = FirebaseVisionImageMetadata.Builder()
            .setWidth(width)
            .setHeight(height)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setRotation(if (cameraFacing == Facing.FRONT) FirebaseVisionImageMetadata.ROTATION_270 else FirebaseVisionImageMetadata.ROTATION_90)
            .build()

        val firebaseVisionImage = FirebaseVisionImage.fromByteArray(frame.data, metadata)

        // <<<<<        >>>>>
        // This are my Detector options!!
        /**
         Do not enable ContourMode and FaceTracking
         ContourMode is only for a single face

         Maybe switch to ContourMode once a single User has been established!!
        **/
        // <<<<<        >>>>>

        val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
            //.setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
            //.setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
            //.setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .build()

        // <<<<<        >>>>>
        // This is where my Detector is invoked!!
        // <<<<<        >>>>>

        val faceDetector = FirebaseVision.getInstance().getVisionFaceDetector(options)
        faceDetector.detectInImage(firebaseVisionImage)
            .addOnSuccessListener {

                Log.v("ActFaceDetect", "Face Found!")

                fd_imgv.setImageBitmap(null)

                val bitmap = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                val dotPaint = Paint()
                dotPaint.color = Color.BLUE
                dotPaint.style = Paint.Style.FILL
                dotPaint.strokeWidth = 4F
                val linePaint = Paint()
                linePaint.color = Color.MAGENTA
                linePaint.style = Paint.Style.STROKE
                linePaint.strokeWidth = 2F
                val textPaint = TextPaint()
                textPaint.color = Color.GREEN
                textPaint.style = Paint.Style.STROKE
                textPaint.textSize = 22F

                val facePaint = Paint()
                facePaint.color = Color.RED
                facePaint.style = Paint.Style.STROKE
                facePaint.strokeWidth = 8F
                val faceTextPaint = Paint()
                faceTextPaint.color = Color.RED
                faceTextPaint.textSize = 40F
                faceTextPaint.typeface = Typeface.DEFAULT_BOLD


                // draw some text on the canvas
                val bounds = Rect()
                textPaint.getTextBounds("STARTING",0, "STARTING".length, bounds)
                var x = (bitmap.width - bounds.width()) / 2f
                var y = (bitmap.height + bounds.width()) / 2f
                canvas.drawText("STARTING", x, 0.0f, textPaint)


                // iterate through the face(s) found with the detector
                //for ((index, face) in it.withIndex()) {
                for (face in it) {

                    faceSize = (face.boundingBox.height() * face.boundingBox.width()) * 1.0f
                    faceSizeUpdate()
                    //canvas.drawRect(face.boundingBox, facePaint)
                    //canvas.drawText("Face$index", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    //canvas.drawText(faceSize.toString(), (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)

                    if (faceNear) {
                        canvas.drawText("NEAR", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }
                    if (faceMid) {
                            canvas.drawText("MID", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }
                    if (faceFar) {
                        canvas.drawText("FAR", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }

                    val faceContours = face.getContour(FirebaseVisionFaceContour.FACE).points
                    for ((i, contour) in faceContours.withIndex()) {
                        if (i != faceContours.lastIndex)
                            canvas.drawLine(contour.x, contour.y, faceContours[i + 1].x, faceContours[i + 1].y, linePaint)
                        else
                            canvas.drawLine(contour.x, contour.y, faceContours[0].x, faceContours[0].y, linePaint)
                        canvas.drawCircle(contour.x, contour.y, 2F, dotPaint)
                    }


                    if (cameraFacing == Facing.FRONT) {
                        val matrix = Matrix()
                        matrix.preScale(-1F, 1F)
                        val flippedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                        fd_imgv.setImageBitmap(flippedBitmap)
                    } else {
                        fd_imgv.setImageBitmap(bitmap)
                    }
                }

            }
            .addOnFailureListener {
                fd_imgv.setImageBitmap(null)
            }
    }

    private fun faceSizeUpdate() {
        // TODO: Update this into a Player Controller
        Log.v("ActFaceDetect", "Updating Size")


        if (faceSize in faceSizeFarLim..faceSizeMidLo){
            faceNear = false
            faceMid = false
            Log.v("ActFaceDetect", "Far")

            faceFar = true
        }

        if (faceSize in faceSizeMidLo..faceSizeMidHi){
            faceFar = false
            faceNear = false
            Log.v("ActFaceDetect", "Mid")

            faceMid = true
        }

        if (faceSize in faceSizeMidHi..faceSizeNearLim){
            faceFar = false
            faceMid = false
            Log.v("ActFaceDetect", "Near")

            faceNear = true
        }
    }

    private fun startCamera() {
        // TODO: Implement CameraX operations
    }

    private fun updateTransform() {
        // TODO: Implement camera viewfinder transformations
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                imageView.post { startCamera() }
            } else {
                Toast.makeText(this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }
}