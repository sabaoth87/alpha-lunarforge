package com.tnk.lunarforgealpha

import android.Manifest
import android.view.*
import android.widget.FrameLayout
import com.unity3d.player.*
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.os.Bundle
import android.text.TextPaint
import android.util.DisplayMetrics
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.tnk.lunarforgealpha.util.cameraview.controls.Facing
import com.tnk.lunarforgealpha.util.cameraview.frame.Frame
import com.tnk.lunarforgealpha.util.cameraview.frame.FrameProcessor
import kotlinx.android.synthetic.main.act_unity.*


// CameraX Permissions
// This is an arbitrary number we are using to keep tab of the permission
// request. Where an app has multiple context for requesting permission,
// this can help differentiate the different contexts
private const val REQUEST_CODE_PERMISSIONS = 10

// This is an array of all the permission specified in the manifest
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

class ActUnityPlayer : AppCompatActivity(), FrameProcessor, LifecycleOwner {

    // val DECLARATION
    private val tag: String = "ActUnityPlayer"
    private val imageView by lazy { findViewById<ImageView>(R.id.unity_imgv)!! }
    //val tvUnity: TextView = findViewById<TextView>(R.id.unity_tv)
    val tvUnity by lazy { findViewById<TextView>(R.id.unity_tv)!! }

    // varIABLE DECLARATION
    private var cameraFacing: Facing = Facing.BACK      // for emulator testing
    //private var cameraFacing: Facing = Facing.FRONT     // for device

    private var faceSize = 0.0f

    private var faceNear = false
    private var faceMid = false
    private var faceFar = false
    var dangerousBroadcaster: String = ""
    var unityResult: String = "waiting"

    var faceInputFar = 100.0f
    var faceInputNear = 1.0f

    var faceSizeNearLim = 150000.0f
    var faceSizeMidLo =  16500.0f
    var faceSizeMidHi =  17500.0f
    var faceSizeFarLim = 400.0
    var facePosition = 0.0;

    private lateinit var mUnityPlayer: UnityPlayer // don't change

    init {
        Log.v(tag, "First init!")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // UNITY INITIALIZE
        setContentView(R.layout.act_unity)
        mUnityPlayer = UnityPlayer(this)
        val glesMode = mUnityPlayer.settings.getInt("gles_mode", 1)
        val trueColor8888 = false
        mUnityPlayer.init(glesMode, trueColor8888)
        val layout = findViewById<View>(R.id.frame_unity) as FrameLayout
        val lp = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        layout.addView(mUnityPlayer.view, 0, lp)
        // UNITY INIT::DONE

        button_set_low.setOnClickListener{
            faceInputNear = faceSize
        }

        button_set_high.setOnClickListener{
            faceInputFar = faceSize
        }


        // Face Finder!!
        unity_camv.facing = cameraFacing
        unity_camv.setLifecycleOwner(this)
        unity_camv.addFrameProcessor(this)

        // CameraX Permissions
        // Add this at the end of onCreate function

        //viewFinder = findViewById(R.id.view_finder)

        // Request camera permissions
        if (allPermissionsGranted()) {
            imageView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                this,
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }

        // Every time the provided texture view changes, recompute layout
        imageView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent)
    }

    // Quit Unity
    override fun onDestroy() {
        mUnityPlayer.quit()
        super.onDestroy()
    }

    // Pause Unity
    override fun onPause() {
        super.onPause()
        mUnityPlayer.pause()
    }

    // Resume Unity
    override fun onResume() {
        super.onResume()
        mUnityPlayer.resume()
    }

    override fun onStart() {
        super.onStart()
        mUnityPlayer.start()
    }

    override fun onStop() {
        super.onStop()
        mUnityPlayer.stop()
    }

    // Low Memory Unity
    override fun onLowMemory() {
        super.onLowMemory()
        mUnityPlayer.lowMemory()
    }

    // Trim Memory Unity
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (level == TRIM_MEMORY_RUNNING_CRITICAL) {
            mUnityPlayer.lowMemory()
        }
    }

    // This ensures the layout will be correct.
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mUnityPlayer.configurationChanged(newConfig)
    }

    // Notify Unity of the focus change.
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        mUnityPlayer.windowFocusChanged(hasFocus)
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event)
        return super.dispatchKeyEvent(event)
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        return mUnityPlayer.injectEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        return mUnityPlayer.injectEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return mUnityPlayer.injectEvent(event)
    }

    /*API12*/ override fun onGenericMotionEvent(event: MotionEvent): Boolean {
        return mUnityPlayer.injectEvent(event)
    }



    // Add this after onCreate

    override fun process(frame: Frame) {

        //val widthOrig = frame.size.width
        //val heightOrig = frame.size.height
        // TODO I believe this height and width should be tied to the size of whatever frame it is drawing onto
        // FIXME
        //val width = 200
        //val height = 200

        val display = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(display)

        val displayWidth = display.widthPixels
        val displayHeight = display.heightPixels

        val width = frame.size.width
        val height = frame.size.height

        val metadata = FirebaseVisionImageMetadata.Builder()
            .setWidth(width)
            .setHeight(height)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setRotation(if (cameraFacing == Facing.FRONT) FirebaseVisionImageMetadata.ROTATION_270 else FirebaseVisionImageMetadata.ROTATION_90)
            .build()

        val firebaseVisionImage = FirebaseVisionImage.fromByteArray(frame.data, metadata)

        // <<<<<        >>>>>
        // This are my Detector options!!
        /**
        Do not enable ContourMode and FaceTracking
        ContourMode is only for a single face

        Maybe switch to ContourMode once a single User has been established!!
         **/
        // <<<<<        >>>>>

        val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
            .build()

        // <<<<<        >>>>>
        // This is where my Detector is invoked!!
        // <<<<<        >>>>>

        val faceDetector = FirebaseVision.getInstance().getVisionFaceDetector(options)
        faceDetector.detectInImage(firebaseVisionImage)
            .addOnSuccessListener {
                Log.v("ActFaceDetect", "Face Found!")

                unity_imgv.setImageBitmap(null)

                val bitmap = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                val dotPaint = Paint()
                dotPaint.color = Color.BLUE
                dotPaint.style = Paint.Style.FILL
                dotPaint.strokeWidth = 4F
                val linePaint = Paint()
                linePaint.color = Color.MAGENTA
                linePaint.style = Paint.Style.STROKE
                linePaint.strokeWidth = 2F
                val textPaint = TextPaint()
                textPaint.color = Color.GREEN
                textPaint.style = Paint.Style.STROKE
                textPaint.textSize = 22F

                val facePaint = Paint()
                facePaint.color = Color.RED
                facePaint.style = Paint.Style.STROKE
                facePaint.strokeWidth = 8F
                val faceTextPaint = Paint()
                faceTextPaint.color = Color.RED
                faceTextPaint.textSize = 40F
                faceTextPaint.typeface = Typeface.DEFAULT_BOLD

                // draw some text on the canvas
                val bounds = Rect()
                textPaint.getTextBounds("STARTING",0, "STARTING".length, bounds)
                var x = (bitmap.width - bounds.width()) / 2f
                var y = (bitmap.height + bounds.width()) / 2f
                canvas.drawText("STARTING", x, 0.0f, textPaint)

                // iterate through the face(s) found with the detector
                //for ((index, face) in it.withIndex()) {
                for (face in it) {

                    faceSize = (face.boundingBox.height() * face.boundingBox.width()) * 1.0f
                    faceSizeUpdate()
                    //canvas.drawRect(face.boundingBox, facePaint)
                    //canvas.drawText("Face$index", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    //canvas.drawText(faceSize.toString(), (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)

                    if (faceNear) {
                        canvas.drawText("NEAR", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }
                    if (faceMid) {
                        canvas.drawText("MID", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }
                    if (faceFar) {
                        canvas.drawText("FAR", (face.boundingBox.centerX() - face.boundingBox.width() / 2) + 8F, (face.boundingBox.centerY() + face.boundingBox.height() / 2) - 8F, faceTextPaint)
                    }

                    val faceContours = face.getContour(FirebaseVisionFaceContour.FACE).points
                    for ((i, contour) in faceContours.withIndex()) {
                        if (i != faceContours.lastIndex)
                            canvas.drawLine(contour.x, contour.y, faceContours[i + 1].x, faceContours[i + 1].y, linePaint)
                        else
                            canvas.drawLine(contour.x, contour.y, faceContours[0].x, faceContours[0].y, linePaint)
                        canvas.drawCircle(contour.x, contour.y, 2F, dotPaint)
                    }


                    if (cameraFacing == Facing.FRONT) {
                        val matrix = Matrix()
                        matrix.preScale(-1F, 1F)
                        val flippedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                        unity_imgv.setImageBitmap(flippedBitmap)
                    } else {
                        unity_imgv.setImageBitmap(bitmap)
                    }
                }

            }
            .addOnFailureListener {
                unity_imgv.setImageBitmap(null)
            }
    }

    private fun startCamera() {
        // TODO: Implement CameraX operations
    }

    private fun updateTransform() {
        // TODO: Implement camera viewfinder transformations
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                //viewFinder.post { startCamera() }
                imageView.post { startCamera() }

            } else {
                Toast.makeText(this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun faceSizeUpdate() {
        // TODO: Update this into a Player Controller
        Log.v("ActFaceDetect", "Updating Size")

        var midpoint = ((faceInputFar + faceInputNear) /2)

        if (faceSize in midpoint..faceInputFar){
            faceNear = false
            Log.v("ActFaceDetect", "Far")
            Log.v("ActFaceDetect", faceSize.toString())

            faceFar = true
        }

        if (faceSize in faceInputNear..midpoint){
            faceFar = false
            Log.v("ActFaceDetect", "Near")
            Log.v("ActFaceDetect", faceSize.toString())

            faceNear = true
        }

        // Broadcast to Unity
         dangerousBroadcaster = UnitySendMessageExtension(
            "Player",
            "receiveAndroid",
            faceSize.toString())
        //tvUnity.text = dangerousBroadcaster
    }

    // <<<<<<<<<<<    >>>>>>>>>>

    // For Unity Interactions !!

    // <<<<<<<<<<<    >>>>>>>>>>

    fun unity_FacePosition(): Double{

        return faceSize.toDouble()

  }

    /**
     * Attempts to send a Message to a Unity object for execution efforts
     */
    fun UnitySendMessageExtension( gameObject: String,
                                   funName: String,
                                   funParam: String): String{
        UnityPlayer.UnitySendMessage( gameObject, funName, funParam)
        // It is implied that the Unity session updates the next variable
        // via
        Log.v("ActFaceDetect", "Sending value to Unity...")

        //val tempResult = unityResult
        return unityResult
    }

    /**
     * Attempts to receive a Message from a Unity object for execution efforts
     */
    fun UnityReceiveResult(value: String): String {
        Log.v("ActFaceDetect", "Receiving Value From Unity...")

        unityResult = "d3rp"    // clear the container
        unityResult = value     // fill it with new
        tvUnity.text = unityResult
        var responder: String = "87"
        return responder
    }

    interface NativeBridge {
        fun getData(key: String, specification: String): String

        fun onMessage(command: String, data: String?){

        }
    }

    object NativeHelper {
        private var bridge: NativeBridge? = null

        fun sendUnityMessage(function: String, message: String = "") {
            UnityPlayer.UnitySendMessage("Director", function, message)
        }

        @JvmStatic
        fun sendCommand(command: String, data: String?) {
            //this.bridge?.onMessage(command, data)
            Log.v("ActFaceDetect", "Receiving Value From Unity...")

            bridge?.onMessage(command, data)
        }

        @JvmStatic
        fun getData(key: String, specification: String): String {
            return bridge?.getData(key, specification) ?: ""
        }
    }
}
